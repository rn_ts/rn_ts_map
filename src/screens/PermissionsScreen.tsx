import React, {useContext} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {PermissionsContext} from '../context/PermissionsContext';
import {Button} from '../components/Button';

export const PermissionsScreen = () => {
  const {permissions, askLocationPermissions} = useContext(PermissionsContext);

  return (
    <View style={styles.container}>
      <Text>PermissionsScreen</Text>
      <Button title="Permission" onPress={askLocationPermissions} />
      <Text>{JSON.stringify(permissions, null, 4)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
