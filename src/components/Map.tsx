import React, {useEffect, useRef, useState} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Polyline} from 'react-native-maps';
import {useLocation} from '../hooks/useLocation';
import {LoadingScreen} from '../screens/LoadingScreen';
import {Fab} from './Fab';

export const Map = () => {
  const {
    routeLines,
    hasLocation,
    userLocation,
    initialPosition,
    getCurrentLocation,
    followUserLocation,
    stopFollowingUserLocation,
  } = useLocation();

  const mapView = useRef<MapView>();
  const isFollowing = useRef<boolean>(true);
  const [isPolylineVisible, setIsPolylineVisible] = useState<boolean>(true);

  useEffect(() => {
    followUserLocation();
    return () => {
      stopFollowingUserLocation();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!isFollowing.current) {
      return;
    }

    mapView.current?.animateCamera({
      center: {
        latitude: userLocation.latitude,
        longitude: userLocation.longitude,
      },
    });
  }, [userLocation]);

  const togglePolyline = () => setIsPolylineVisible(!isPolylineVisible);

  const centerPosition = async () => {
    isFollowing.current = true;
    const {latitude, longitude} = await getCurrentLocation();
    mapView.current?.animateCamera({
      center: {
        latitude,
        longitude,
      },
    });
  };

  if (!hasLocation) {
    <LoadingScreen />;
  }
  return (
    <>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        showsUserLocation
        ref={el => (mapView.current = el!)}
        region={{
          latitude: initialPosition.latitude,
          longitude: initialPosition.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
        onTouchStart={() => (isFollowing.current = false)}>
        {isPolylineVisible && (
          <Polyline
            coordinates={routeLines}
            strokeColor="black"
            strokeWidth={3}
          />
        )}
        {/* <Marker
          image={require('../assets/custom-marker.png')}
          coordinate={{
            latitude: 37.78825,
            longitude: -122.4324,
          }}
          title={'Title'}
          description={'Description'}
        /> */}
      </MapView>
      <Fab
        iconName="brush-outline"
        onPress={togglePolyline}
        style={styles.polylineButton}
      />
      <Fab
        iconName="compass-outline"
        onPress={centerPosition}
        style={styles.button}
      />
    </>
  );
};

let {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  map: {
    flex: 1,
    width: width,
    height: height,
  },
  button: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
  polylineButton: {
    position: 'absolute',
    bottom: 80,
    right: 20,
  },
});
