import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props {
  style?: StyleProp<ViewStyle>;
  iconName: string;
  onPress: () => void;
}

export const Fab = ({iconName, onPress, style}: Props) => {
  return (
    <View style={[style]}>
      <TouchableOpacity
        style={[styles.button]}
        activeOpacity={0.8}
        onPress={onPress}>
        <Icon name={iconName} color={'white'} size={25} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    zIndex: 999,
    height: 50,
    width: 50,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0.3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.5,
    elevation: 6,
  },
});
